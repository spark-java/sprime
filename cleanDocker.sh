#!/bin/bash

echo "Deleting all Docker containers..."
if [ "$(docker ps -aq)" ]; then
    docker rm -f $(docker ps -aq)
else
    echo "No containers to remove."
fi

# Delete all Docker images
echo "Deleting all Docker images..."
if [ "$(docker images -aq)" ]; then
   docker rmi $(docker images -aq)
else
    echo "No images to remove."
fi

# Delete all Docker volumes
echo "Deleting all Docker volumes..."
if [ "$(docker volume ls -q)" ]; then
   docker volume rm $(docker volume ls -q)
else
    echo "No volumes to remove."
fi

# Delete all Docker networks except for the pre-defined ones
echo "Deleting all Docker networks..."
if [ "$(docker network ls --filter type=custom -q)" ]; then
   docker network rm $(docker network ls --filter type=custom -q)
else
    echo "No custom networks to remove."
fi

docker builder prune --force

echo "Done!"

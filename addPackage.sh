#!/bin/bash

# Function to capitalize the first letter of a word
capitalize() {
    local word="$1"
    echo "$(tr '[:lower:]' '[:upper:]' <<<"${word:0:1}")${word:1}"
}

# Function to create a Java class file based on the provided directory and class name
createJavaClass() {
    local dir="$1"
    local targetDir="$2"
    local fileName="$3"
# Create a Java class file
    case $dir in
        controllers)
            touch "$targetDir/${fileName}Controller.java"
            ;;
        models)
            touch "$targetDir/${fileName}.java"
            ;;
        repositories)
            touch "$targetDir/${fileName}Repository.java"
            ;;
        services)
            touch "$targetDir/${fileName}Service.java"
            ;;
    esac
}

# Check if the user provided a root directory as a command-line argument
if [ -z "$1" ]; then
    echo "Please enter package name"
    exit 1
fi

# Capitalize the provided package name
capitalized_word=$(capitalize "$1")

base_directory=src/main/java

base_directory=$(ls -d "$base_directory"/*/)

base_directory=$(ls -d "$base_directory"/*/)

# Construct the full path for the new package
package_directory="$base_directory/app/$1"

# Create the package directory
mkdir -p "$package_directory"

# Directories to create within the package
dirs=("controllers" "models" "repositories" "services")

# Loop through the directories and create Java class files
for dir in "${dirs[@]}"; do
    # Capitalize the directory name
    capitalized_dir=$(capitalize "$dir")

    targetDir="$package_directory/$dir"
    # Create the directory
    mkdir -p "$targetDir"
    createJavaClass "$dir" "$targetDir" "$capitalized_word"

done

# Display a message indicating the structure has been created
echo "The new package $1 has been created."

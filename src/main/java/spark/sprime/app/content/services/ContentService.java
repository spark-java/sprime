package spark.sprime.app.content.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import spark.sprime.app.common.models.Backdrop;
import spark.sprime.app.common.models.Genre;
import spark.sprime.app.common.models.Logo;
import spark.sprime.app.common.models.Poster;
import spark.sprime.app.common.repositories.GenreRepository;
import spark.sprime.app.content.dto.MovieResponse;
import spark.sprime.app.content.enums.ContentType;
import spark.sprime.app.content.models.Content;
import spark.sprime.app.content.repositories.ContentRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ContentService {

  @Autowired
  private ContentRepository contentRepository; // Assuming you have a repository for Content

  @Autowired
  private GenreRepository genreRepository;

  private MovieResponse mapContentToMovieResponse(Content obj) {
    MovieResponse movieResponse = new MovieResponse();
    movieResponse.setAdult(obj.isAdult());
    movieResponse.setBackdropPath(obj.getBackdropPath());
    movieResponse.setBudget(obj.getBudget());
    movieResponse.setGenres(obj.getGenres());
    movieResponse.setHomepage(obj.getHomepage());
    movieResponse.setImdbId(obj.getImdbId());
    movieResponse.setOriginalLanguage(obj.getOriginalLanguage());
    movieResponse.setOriginalTitle(obj.getOriginalTitle());
    movieResponse.setOverview(obj.getOverview());
    movieResponse.setPopularity(obj.getPopularity());
    movieResponse.setPosterPath(obj.getPosterPath());
    movieResponse.setRevenue(obj.getRevenue());
    movieResponse.setRuntime(obj.getRuntime());
    movieResponse.setStatus(obj.getStatus());
    movieResponse.setTagline(obj.getTagline());
    movieResponse.setTitle(obj.getTitle());
    movieResponse.setVoteAverage(obj.getVoteAverage());
    movieResponse.setVoteCount(obj.getVoteCount());
    movieResponse.setProductionCompanies(obj.getProductionCompanies());
    movieResponse.setProductionCountries(obj.getProductionCountries());
    movieResponse.setSpokenLanguages(obj.getSpokenLanguages());
    movieResponse.setCast(obj.getCast());
    movieResponse.setCrew(obj.getCrew());
    movieResponse.setSpokenLanguages(obj.getSpokenLanguages());
    movieResponse.setSpokenLanguages(obj.getSpokenLanguages());
    return movieResponse;

  }

  public List<Content> getAllContent() {
    return contentRepository.findAll();
  }

  public Optional<Content> getContentByIdAndType(Long id, ContentType type) {
    return contentRepository.findByIdAndType(id, type);
  }

  public Optional<MovieResponse> GetMovieById(Long id) {
    Optional<Content> content = contentRepository.findByIdAndType(id, ContentType.Movie);
    if (content != null) {
      Content obj = content.get();
      return Optional.of(this.mapContentToMovieResponse(obj));
    }
    return null;
  }

  @Transactional
  public MovieResponse saveMovie(Content content) {
    content.setType(ContentType.Movie);
    // Save posters
    if (content.getPosters() != null) {
      for (Poster poster : content.getPosters()) {
        poster.setContent(content);
      }
    }

    // Save logos
    if (content.getLogos() != null) {
      for (Logo logo : content.getLogos()) {
        logo.setContent(content);
      }
    }

    // Save logos
    if (content.getBackdrops() != null) {
      for (Backdrop backdrops : content.getBackdrops()) {
        backdrops.setContent(content);
      }
    }

    Content savedContent = contentRepository.save(content);
    System.out.println("=================" + savedContent);
    return this.mapContentToMovieResponse(savedContent);
  }

  @Transactional
  public Content saveContent(Content content) {
    // Extract unique genres using a more explicit method
    Set<Genre> uniqueGenres = extractUniqueGenres(content);

    // Detach genres from content temporarily
    content.setGenres(null);

    // Save content without genres
    Content savedContent = contentRepository.save(content);

    // Initialize the genres list if it's null
    if (savedContent.getGenres() == null) {
      savedContent.setGenres(new ArrayList<>());
    }

    // Save unique genres and associate them with the saved content
    associateGenresWithContent(uniqueGenres, savedContent);

    // Update content with the saved genres
    return contentRepository.save(savedContent);
  }

  private Set<Genre> extractUniqueGenres(Content content) {
    return new HashSet<>(content.getGenres());
  }

  private void associateGenresWithContent(Set<Genre> uniqueGenres, Content savedContent) {
    for (Genre genre : uniqueGenres) {
      Genre savedGenre = genreRepository.findByName(genre.getName());

      if (savedGenre == null) {
        savedGenre = genreRepository.save(genre);
      }

      // Check and initialize the genres list if it's null
      if (savedContent.getGenres() == null) {
        savedContent.setGenres(new ArrayList<>());
      }

      savedContent.getGenres().add(savedGenre);
    }
  }

  public void deleteContent(Long id) {
    contentRepository.deleteById(id);
  }

}

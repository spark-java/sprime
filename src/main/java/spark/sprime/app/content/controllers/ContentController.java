package spark.sprime.app.content.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import spark.sprime.app.content.enums.ContentType;
import spark.sprime.app.content.models.Content;
import spark.sprime.app.content.services.ContentService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/content")
public class ContentController {

  @Autowired
  private ContentService contentService;

  @GetMapping
  public ResponseEntity<List<Content>> getAllContent() {
    List<Content> contentList = contentService.getAllContent();
    return new ResponseEntity<>(contentList, HttpStatus.OK);
  }

  @GetMapping("/{id}")
  public ResponseEntity<Content> getContentById(@PathVariable Long id) {
    Optional<Content> content = contentService.getContentByIdAndType(id, ContentType.Movie);
    return content.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping
  public ResponseEntity<Content> saveContent(@RequestBody Content content) {
    Content savedContent = contentService.saveContent(content);
    return new ResponseEntity<>(savedContent, HttpStatus.CREATED);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteContent(@PathVariable Long id) {
    contentService.deleteContent(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  // Additional methods can be added based on your specific needs

}

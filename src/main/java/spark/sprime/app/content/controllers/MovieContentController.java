package spark.sprime.app.content.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import spark.sprime.app.content.dto.MovieResponse;
import spark.sprime.app.content.models.Content;
import spark.sprime.app.content.services.ContentService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/movie")
public class MovieContentController {

  @Autowired
  private ContentService contentService;

  @GetMapping
  public ResponseEntity<List<Content>> getAllMovies() {
    try {
      List<Content> contentList = contentService.getAllContent();
      System.out.println(contentList.toString());

      if (contentList.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(contentList, HttpStatus.OK);
    } catch (Exception e) {
      // Log the exception using a logging framework
      System.out.println("Error while getting all movies: " + e.toString());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/{id}")
  public ResponseEntity<MovieResponse> GetMovieById(@PathVariable Long id) {
    Optional<MovieResponse> content = contentService.GetMovieById(id);

    return content
        .map(value -> ResponseEntity.ok().body(value))
        .orElse(ResponseEntity.notFound().build());
  }

  @PostMapping
  public ResponseEntity<MovieResponse> saveMovie(@RequestBody Content content) {
    MovieResponse savedContent = contentService.saveMovie(content);
    return new ResponseEntity<MovieResponse>(savedContent, HttpStatus.CREATED);
  }

}

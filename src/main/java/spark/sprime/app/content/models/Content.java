package spark.sprime.app.content.models;

import spark.sprime.app.common.models.*;
import spark.sprime.app.content.enums.ContentType;
import lombok.Data;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "content")
@Data
public class Content {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "budget")
  private int budget;

  @Column(name = "imdb_id", length = 255)
  private String imdbId;

  @Column(name = "original_title", length = 255)
  private String originalTitle;

  @Column(name = "revenue")
  private int revenue;

  @Column(name = "runtime")
  private int runtime;

  @Column(name = "release_date")
  private LocalDate releaseDate;

  @Column(name = "video")
  private boolean video;

  @NotBlank
  @Column(name = "title", length = 255)
  private String title;

  @Column(name = "adult")
  private boolean adult;

  @Size(max = 255)
  @Column(name = "backdrop_path", length = 255)
  private String backdropPath;

  @Size(max = 255)
  @Column(name = "homepage", length = 255)
  private String homepage;

  @Column(name = "original_language", length = 255)
  private String originalLanguage;

  @Column(name = "overview", length = 1000)
  private String overview;

  @Column(name = "popularity")
  private double popularity;

  @Size(max = 255)
  @Column(name = "poster_path", length = 255)
  private String posterPath;

  @Size(max = 50)
  @Column(name = "status", length = 50)
  private String status;

  @Size(max = 255)
  @Column(name = "tagline", length = 255)
  private String tagline;

  @Column(name = "vote_average")
  private double voteAverage;

  @Column(name = "vote_count")
  private int voteCount;

  private List<Integer> episodeRunTime;

  @Column(name = "first_air_date", length = 255)
  private String firstAirDate;

  @Column(name = "in_production")
  private boolean inProduction;

  private List<String> languages;

  private String name;

  @Column(name = "number_of_episodes")
  private int numberOfEpisodes;

  @Column(name = "number_of_seasons")
  private int numberOfSeasons;

  @Column(name = "origin_country")
  private List<String> originCountry;

  @Column(name = "original_name")
  private String originalName;

  private ContentType type;

  @OneToMany(mappedBy = "content", cascade = CascadeType.ALL)
  private List<Backdrop> backdrops;

  @OneToMany(mappedBy = "content", cascade = CascadeType.ALL)
  private List<Logo> logos;

  @OneToMany(mappedBy = "content", cascade = CascadeType.ALL)
  private List<Poster> posters;

  @ManyToMany(targetEntity = Genre.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinTable(name = "content_genre", joinColumns = @JoinColumn(name = "content_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "genre_id", referencedColumnName = "id"))
  private List<Genre> genres;

  @ManyToMany(targetEntity = ProductionCompany.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinTable(name = "content_production_companies", joinColumns = @JoinColumn(name = "content_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "production_company_id", referencedColumnName = "id"))
  private List<ProductionCompany> productionCompanies;

  @ManyToMany(targetEntity = ProductionCountry.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinTable(name = "content_production_country", joinColumns = @JoinColumn(name = "content_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "production_country_id", referencedColumnName = "id"))
  private List<ProductionCountry> productionCountries;

  @ManyToMany(targetEntity = SpokenLanguage.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinTable(name = "content_spoken_language", joinColumns = @JoinColumn(name = "content_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "spoken_language_id", referencedColumnName = "id"))
  private List<SpokenLanguage> spokenLanguages;

  @ManyToMany(targetEntity = Actor.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinTable(name = "content_actor", joinColumns = @JoinColumn(name = "content_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "actor_id", referencedColumnName = "id"))
  private List<Actor> cast;

  @ManyToMany(targetEntity = Crew.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinTable(name = "content_crew", joinColumns = @JoinColumn(name = "content_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "crew_id", referencedColumnName = "id"))
  private List<Crew> crew;

}
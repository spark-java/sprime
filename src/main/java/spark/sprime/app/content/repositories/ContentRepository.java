package spark.sprime.app.content.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import spark.sprime.app.content.enums.ContentType;
import spark.sprime.app.content.models.Content;

public interface ContentRepository extends JpaRepository<Content, Long> {
  Optional<Content> findByIdAndType(Long id, ContentType type);

}

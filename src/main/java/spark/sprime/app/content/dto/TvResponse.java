package spark.sprime.app.content.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonProperty;

import spark.sprime.app.common.models.*;

@Data
@AllArgsConstructor
public class TvResponse {
  private boolean adult;

  @JsonProperty("backdrop_path")
  private String backdropPath;

  @JsonProperty("created_by")
  private List<Object> createdBy;

  @JsonProperty("episode_run_time")
  private List<Integer> episodeRunTime;

  @JsonProperty("first_air_date")
  private String firstAirDate;

  private List<Genre> genres;

  private String homepage;

  private int id;

  @JsonProperty("in_production")
  private boolean inProduction;

  private List<String> languages;

  @JsonProperty("last_air_date")
  private String lastAirDate;

  @JsonProperty("last_episode_to_air")
  private Episode lastEpisodeToAir;

  private String name;

  @JsonProperty("next_episode_to_air")
  private Object nextEpisodeToAir;

  private List<Network> networks;

  @JsonProperty("number_of_episodes")
  private int numberOfEpisodes;

  @JsonProperty("number_of_seasons")
  private int numberOfSeasons;

  @JsonProperty("origin_country")
  private List<String> originCountry;

  @JsonProperty("original_language")
  private String originalLanguage;

  @JsonProperty("original_name")
  private String originalName;

  private String overview;

  private double popularity;

  @JsonProperty("poster_path")
  private String posterPath;

  @JsonProperty("production_companies")
  private List<ProductionCompany> productionCompanies;

  @JsonProperty("production_countries")
  private List<ProductionCountry> productionCountries;

  private List<Season> seasons;

  @JsonProperty("spoken_languages")
  private List<SpokenLanguage> spokenLanguages;

  private String status;

  private String tagline;

  private String type;

  @JsonProperty("vote_average")
  private double voteAverage;

  @JsonProperty("vote_count")
  private int voteCount;

  private List<Video> videos;
  private List<Actor> cast;
  private List<Crew> crew;
  private List<Backdrop> backdrops;
  private List<Logo> logos;
  private List<Poster> posters;
}

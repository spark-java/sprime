package spark.sprime.app.content.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import spark.sprime.app.common.models.*;

import com.fasterxml.jackson.annotation.JsonProperty;

@Data
@AllArgsConstructor
public class MovieResponse {

  public MovieResponse() {
    super();
  }

  private boolean adult;

  @JsonProperty("backdrop_path")
  private String backdropPath;

  private int budget;

  private List<Genre> genres;

  private String homepage;

  private int tmdbId;

  @JsonProperty("imdb_id")
  private String imdbId;

  @JsonProperty("original_language")
  private String originalLanguage;

  @JsonProperty("original_title")
  private String originalTitle;

  private String overview;

  private double popularity;

  @JsonProperty("poster_path")
  private String posterPath;

  @JsonProperty("production_companies")
  private List<ProductionCompany> productionCompanies;

  @JsonProperty("production_countries")
  private List<ProductionCountry> productionCountries;

  @JsonProperty("release_date")
  private String releaseDate;

  private int revenue;

  private int runtime;

  @JsonProperty("spoken_languages")
  private List<SpokenLanguage> spokenLanguages;

  private String status;

  private String tagline;

  private String title;

  @JsonProperty("video")
  private boolean isVideo;

  @JsonProperty("vote_average")
  private double voteAverage;

  @JsonProperty("vote_count")
  private int voteCount;

  private List<Video> videos;
  private List<Actor> cast;
  private List<Crew> crew;
  private List<Backdrop> backdrops;
  private List<Logo> logos;
  private List<Poster> posters;

}

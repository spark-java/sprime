package spark.sprime.app.common.enums;

public enum ImageType {
  Backdrop,
  Logo,
  Poster
}

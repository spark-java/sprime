package spark.sprime.app.common.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spark.sprime.app.common.models.Genre;
import spark.sprime.app.common.repositories.GenreRepository;

import java.util.List;
import java.util.Optional;

@Service
public class GenreService {

  private final GenreRepository genreRepository;

  @Autowired
  public GenreService(GenreRepository genreRepository) {
    this.genreRepository = genreRepository;
  }

  // Save a genre
  public Genre saveGenre(Genre genre) {
    return genreRepository.save(genre);
  }

  // Get all genres
  public List<Genre> getAllGenres() {
    return genreRepository.findAll();
  }

  // Get a genre by ID
  public Optional<Genre> getGenreById(Long id) {
    return genreRepository.findById(id);
  }

  // Delete a genre by ID
  public void deleteGenreById(Long id) {
    genreRepository.deleteById(id);
  }

  // Other methods as needed for your application logic

}

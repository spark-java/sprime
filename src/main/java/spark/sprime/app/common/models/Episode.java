package spark.sprime.app.common.models;

import jakarta.persistence.*;
import lombok.Data;
import spark.sprime.app.content.models.Content;

@Table(name = "episode")
@Data
public class Episode {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;
  private String overview;
  private int runtime;
  private double voteAverage;
  private int voteCount;
  private String airDate;
  private int episodeNumber;
  private String episodeType;
  private String productionCode;
  private int seasonNumber;
  private int showId;
  private String stillPath;

  @ManyToOne
  @JoinColumn(name = "content_id")
  private Content content;

  // Getters and setters...
}
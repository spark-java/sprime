package spark.sprime.app.common.models;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import spark.sprime.app.common.enums.ImageType;
import spark.sprime.app.content.models.Content;

@Data
@MappedSuperclass
public class BaseImage {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private double aspect_ratio;

  private int height;

  private String iso_639_1;

  private String file_path;

  private double vote_average;

  private int vote_count;

  private int width;

  private ImageType type;

  @ManyToOne(targetEntity = Content.class)
  @JoinColumn(name = "content_id", referencedColumnName = "id")
  private Content content;
}

package spark.sprime.app.common.models;

import spark.sprime.app.content.models.Content;

import jakarta.persistence.*;
import lombok.Data;

@Table(name = "season")
@Data
public class Season {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;
  private String overview;
  private String airDate;
  private int episodeCount;
  private String posterPath;
  private int seasonNumber;
  private double voteAverage;

  @ManyToOne
  @JoinColumn(name = "content_id")
  private Content content;

}
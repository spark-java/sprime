package spark.sprime.app.common.models;

import jakarta.persistence.*;
import lombok.Data;
import spark.sprime.app.content.models.Content;

@Table(name = "created_by")
@Data
public class CreatedBy {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private int gender;
  private String name;

  @ManyToMany(mappedBy = "owner")
  private Content content;

}
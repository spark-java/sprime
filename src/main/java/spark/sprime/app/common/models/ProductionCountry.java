package spark.sprime.app.common.models;

import java.util.List;
import spark.sprime.app.content.models.Content;
import jakarta.persistence.*;
import lombok.Data;

@Table(name = "production_country")
@Data
@Entity
public class ProductionCountry {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;

  private String iso_3166_1;

  @ManyToMany(mappedBy = "productionCountries")
  private List<Content> content;

  // Getters and setters...
}
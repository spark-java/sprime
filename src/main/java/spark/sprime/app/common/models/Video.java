package spark.sprime.app.common.models;

import jakarta.persistence.*;
import lombok.Data;
import spark.sprime.app.content.models.Content;

@Table(name = "video")
@Data
public class Video {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String iso639_1;
  private String iso3166_1;
  private String name;
  private String key;
  private String site;
  private int size;
  private String type;
  private boolean official;

  @Column(name = "published_at", nullable = false, length = 512)
  private String publishedAt;

  @ManyToOne
  @JoinColumn(name = "content_id")
  private Content content;

}
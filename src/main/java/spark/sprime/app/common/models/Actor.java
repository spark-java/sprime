package spark.sprime.app.common.models;

import spark.sprime.app.content.models.Content;
import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Table(name = "actor")
@Data
@Entity
public class Actor {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private boolean adult;
  private int gender;
  private String name;
  private String original_name;
  private String known_for_department;
  private double popularity;
  private String character;
  private int cast_id;
  private String profile_path;
  private String credit_id;

  @ManyToMany(mappedBy = "cast")
  private List<Content> content;
}
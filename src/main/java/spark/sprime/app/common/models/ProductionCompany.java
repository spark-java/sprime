package spark.sprime.app.common.models;

import java.util.List;
import spark.sprime.app.content.models.Content;
import jakarta.persistence.*;
import lombok.Data;

@Table(name = "production_company")
@Data
@Entity
public class ProductionCompany {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;

  private String logo_path;

  private String origin_country;

  @ManyToMany(mappedBy = "productionCompanies")
  private List<Content> content;

  // Getters and Listters...
}
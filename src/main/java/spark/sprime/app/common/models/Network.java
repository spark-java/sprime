package spark.sprime.app.common.models;

import java.util.List;
import spark.sprime.app.content.models.Content;
import jakarta.persistence.*;
import lombok.Data;

@Table(name = "network")
@Data
public class Network {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", nullable = false, length = 512)
  private String name;

  @Column(name = "logo_path", nullable = false, length = 512)
  private String logoPath;

  @Column(name = "origin_country", nullable = false, length = 512)
  private String originCountry;

  @ManyToMany(mappedBy = "networks")
  private List<Content> content;

  // Getters and Listters...
}
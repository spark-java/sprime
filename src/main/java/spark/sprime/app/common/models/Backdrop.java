package spark.sprime.app.common.models;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Table(name = "backdrop")
@EqualsAndHashCode(callSuper = false)
@Data
@Entity
public class Backdrop extends BaseImage {

}

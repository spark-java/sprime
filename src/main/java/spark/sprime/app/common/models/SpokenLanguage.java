package spark.sprime.app.common.models;

import java.util.List;
import spark.sprime.app.content.models.Content;
import lombok.Data;
import jakarta.persistence.*;

@Table(name = "spoken_language")
@Data
@Entity
public class SpokenLanguage {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;

  private String english_name;

  private String iso_639_1;

  @ManyToMany(mappedBy = "spokenLanguages")
  private List<Content> content;

  // Getters and setters...
}

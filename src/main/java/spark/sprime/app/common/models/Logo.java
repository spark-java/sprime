package spark.sprime.app.common.models;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Table(name = "logo")
@EqualsAndHashCode(callSuper = false)
@Data
@Entity
public class Logo extends BaseImage {

}
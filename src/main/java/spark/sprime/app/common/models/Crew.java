package spark.sprime.app.common.models;

import java.util.List;

import spark.sprime.app.content.models.Content;
import jakarta.persistence.*;
import lombok.Data;

@Table(name = "crew")
@Data
@Entity
public class Crew {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private boolean adult;

  private int gender;

  private String name;

  private String known_for_department;

  private String original_name;

  private double popularity;

  private String profile_path;

  private String credit_id;

  private String department;

  private String job;

  @ManyToMany(mappedBy = "crew")
  private List<Content> content;

  // Getters and setters...
}
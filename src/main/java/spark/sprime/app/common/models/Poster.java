package spark.sprime.app.common.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Table(name = "poster")
@EqualsAndHashCode(callSuper = false)
@Data
@Entity
public class Poster extends BaseImage {
}
package spark.sprime.app.common.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import spark.sprime.app.common.models.Genre;

public interface GenreRepository extends JpaRepository<Genre, Long> {

  Genre findByName(String name);
  // You can add custom queries if needed
}

package spark.sprime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SprimeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SprimeApplication.class, args);
	}

}

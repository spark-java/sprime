package spark.sprime.tmdb.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "tmdb")
public class TmdbConfig {

  private String base_url;
  private String api_key;
  private String bearer_token;
  private String img_url;

  public String getBaseUrl() {
    return base_url;
  }

  public void setBaseUrl(String base_url) {

    this.base_url = base_url;
  }

  public String getApiKey() {
    return api_key;
  }

  public void setApiKey(String api_key) {
    this.api_key = api_key;
  }

  public String getBearerToken() {
    return bearer_token;
  }

  public void setBearerToken(String bearer_token) {
    this.bearer_token = bearer_token;
  }

  public String getImgUrl() {
    return img_url;
  }

  public void setImgUrl(String img_url) {
    this.img_url = img_url;
  }

}

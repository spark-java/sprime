package spark.sprime.tmdb.commons;

import java.util.List;

import lombok.Data;

@Data
public class Credits {
  private static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/original";

  @Data
  public static class Cast {
    private boolean adult;
    private int gender;
    private int id;
    private String known_for_department;
    private String name;
    private String original_name;
    private double popularity;
    private String profile_path;
    private int cast_id;
    private String character;
    private String credit_id;
    private int order;

    public String getProfile_path() {
      if (this.profile_path != null) {
        return IMAGE_BASE_URL + this.profile_path;
      } else {
        return null;
      }
    }
  }

  @Data
  public static class Crew {
    private boolean adult;
    private int gender;
    private int id;
    private String known_for_department;
    private String name;
    private String original_name;
    private double popularity;
    private String profile_path;
    private String credit_id;
    private String department;
    private String job;

    public String getProfile_path() {
      if (this.profile_path != null) {
        return IMAGE_BASE_URL + this.profile_path;
      } else {
        return null;
      }
    }
  }

  private List<Cast> cast;
  private List<Crew> crew;
}

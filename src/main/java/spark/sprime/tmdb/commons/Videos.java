package spark.sprime.tmdb.commons;

import java.util.List;

import lombok.Data;

@Data
public class Videos {
  @Data
  public static class Result {
    private String iso_639_1;
    private String iso_3166_1;
    private String name;
    private String key;
    private String site;
    private int size;
    private String type;
    private boolean official;
    private String published_at;
    private String id;

  }

  private List<Result> results;

  // Getters and setters
}

package spark.sprime.tmdb.commons;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SpokenLanguageDTO {
  @JsonProperty("english_name")
  private String englishName;

  @JsonProperty("iso_639_1")
  private String iso6391;

  private String name;
}
package spark.sprime.tmdb.commons;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

// Nested classes for inner structures
@Data
public class CollectionDTO {
  private static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/original";

  private int id;

  private String name;

  @JsonProperty("poster_path")
  private String posterPath;

  @JsonProperty("backdrop_path")
  private String backdropPath;

  public String getPosterPath() {
    if (this.posterPath != null) {
      return IMAGE_BASE_URL + this.posterPath;
    } else {
      return null;
    }
  }

  public String getBackdropPath() {
    if (this.backdropPath != null) {
      return IMAGE_BASE_URL + this.backdropPath;
    } else {
      return null;
    }
  }
}
package spark.sprime.tmdb.commons;

import lombok.Data;

@Data
public class GenreDTO {
  private int id;

  private String name;
}
package spark.sprime.tmdb.commons;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class NetworkDTO {
  private static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/original";

  private int id;
  @JsonProperty("logo_path")
  private String logoPath;
  private String name;
  @JsonProperty("origin_country")
  private String originCountry;

  public String getLogoPath() {
    if (this.logoPath != null) {
      return IMAGE_BASE_URL + this.logoPath;
    } else {
      return null;
    }
  }
}
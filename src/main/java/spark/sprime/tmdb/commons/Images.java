package spark.sprime.tmdb.commons;

import java.util.List;

import lombok.Data;

@Data
public class Images {

  private static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/original";

  @Data
  public static class Image {
    private double aspect_ratio;
    private int height;
    private String iso_639_1;
    private String file_path;
    private double vote_average;
    private int vote_count;
    private int width;

    public String getFile_path() {
      if (this.file_path != null) {
        return IMAGE_BASE_URL + this.file_path;
      } else {
        return null;
      }
    }
  }

  private List<Image> backdrops;
  private List<Image> logos;
  private List<Image> posters;
}

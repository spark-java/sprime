package spark.sprime.tmdb.movie.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MovieDTO {
  private static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/original";

  private boolean adult;
  @JsonProperty("backdrop_path")
  private String backdropPath;
  @JsonProperty("genres")
  private List<Integer> genreIds;
  private int id;
  @JsonProperty("original_language")
  private String originalLanguage;
  @JsonProperty("original_title")
  private String originalTitle;
  private String overview;
  private double popularity;
  @JsonProperty("poster_path")
  private String posterPath;

  @JsonProperty("release_date")
  @JsonFormat(pattern = "yyyy-MM-dd")
  private String releaseDate;

  private String title;
  private boolean video;
  @JsonProperty("vote_average")
  private double voteAverage;
  @JsonProperty("vote_count")
  private int voteCount;

  public String getPosterPath() {
    if (this.posterPath != null) {
      return IMAGE_BASE_URL + this.posterPath;
    } else {
      return null;
    }
  }

  public String getBackdropPath() {
    if (this.backdropPath != null) {
      return IMAGE_BASE_URL + this.backdropPath;
    } else {
      return null;
    }
  }

}
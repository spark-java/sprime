package spark.sprime.tmdb.movie.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import spark.sprime.tmdb.commons.*;
import spark.sprime.tmdb.commons.Credits.*;
import spark.sprime.tmdb.commons.Images.Image;

import java.util.List;

@Data
@AllArgsConstructor
public class MovieDetailsResultDTO {
  public MovieDetailsResultDTO() {
    super();
  }

  private static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/original";

  private boolean adult;

  private String backdropPath;

  private int budget;

  private List<GenreDTO> genres;

  private String homepage;

  private int tmdbId;

  private String imdbId;

  private String originalLanguage;

  private String originalTitle;

  private String overview;

  private double popularity;

  private String posterPath;

  private List<ProductionCompanyDTO> productionCompanies;

  private List<ProductionCountryDTO> productionCountries;

  private String releaseDate;

  private int revenue;

  private int runtime;

  private List<SpokenLanguageDTO> spokenLanguages;

  private String status;

  private String tagline;

  private String title;

  private boolean isVideo;

  private double voteAverage;

  private int voteCount;

  private List<Videos.Result> videos;
  private List<Cast> cast;
  private List<Crew> crew;
  private List<Image> backdrops;
  private List<Image> logos;
  private List<Image> posters;

  public String getPosterPath() {
    if (this.posterPath != null) {
      return IMAGE_BASE_URL + this.posterPath;
    } else {
      return null;
    }
  }

  public String getBackdropPath() {
    if (this.backdropPath != null) {
      return IMAGE_BASE_URL + this.backdropPath;
    } else {
      return null;
    }
  }
}

package spark.sprime.tmdb.movie.dto;

import java.util.List;

import lombok.Data;

@Data
public class MovieSearchDto {
  private int page;
  private List<MovieDTO> results;
  private int totalPages;
  private int totalResults;

}
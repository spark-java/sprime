package spark.sprime.tmdb.movie.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import spark.sprime.tmdb.commons.*;

import java.util.List;

@Data
public class MovieDetailsDTO {
  private static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/original";

  private boolean adult;

  @JsonProperty("backdrop_path")
  private String backdropPath;

  @JsonProperty("belongs_to_collection")
  private CollectionDTO belongsToCollection;

  private int budget;

  private List<GenreDTO> genres;

  private String homepage;

  private int id;

  @JsonProperty("imdb_id")
  private String imdbId;

  @JsonProperty("original_language")
  private String originalLanguage;

  @JsonProperty("original_title")
  private String originalTitle;

  private String overview;

  private double popularity;

  @JsonProperty("poster_path")
  private String posterPath;

  @JsonProperty("production_companies")
  private List<ProductionCompanyDTO> productionCompanies;

  @JsonProperty("production_countries")
  private List<ProductionCountryDTO> productionCountries;

  @JsonProperty("release_date")
  private String releaseDate;

  private int revenue;

  private int runtime;

  @JsonProperty("spoken_languages")
  private List<SpokenLanguageDTO> spokenLanguages;

  private String status;

  private String tagline;

  private String title;

  @JsonProperty("video")
  private boolean isVideo;

  @JsonProperty("vote_average")
  private double voteAverage;

  @JsonProperty("vote_count")
  private int voteCount;

  private Videos videos;
  private Credits credits;
  private Images images;

  public String getPosterPath() {
    if (this.posterPath != null) {
      return IMAGE_BASE_URL + this.posterPath;
    } else {
      return null;
    }
  }

  public String getBackdropPath() {
    if (this.backdropPath != null) {
      return IMAGE_BASE_URL + this.backdropPath;
    } else {
      return null;
    }
  }
}

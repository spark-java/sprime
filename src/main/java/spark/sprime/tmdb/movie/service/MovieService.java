package spark.sprime.tmdb.movie.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import reactor.core.publisher.Mono;
import spark.sprime.tmdb.api.TmdbClient;
import spark.sprime.tmdb.movie.dto.*;

/**
 * MovieService
 */
@Service
public class MovieService {
  private static final Logger logger = Logger.getLogger(MovieService.class.getName());

  @Autowired
  private TmdbClient tmdbClient;

  public List<MovieDTO> searchMovies(String query) {
    return tmdbClient.getWebClient().get()
        .uri(uriBuilder -> uriBuilder.path("/search/movie").queryParam("query", query).build())
        .retrieve()
        .onStatus(
            status -> status.value() == 404,
            clientResponse -> {
              logger.warning("Movie not found");
              return Mono.empty();
            })
        .bodyToMono(MovieSearchDto.class)
        .map(MovieSearchDto::getResults)
        .onErrorResume(e -> {
          logger.warning("Error making a request to TMDb API: " + e.getMessage());
          return Mono.just(Collections.emptyList());
        })
        .block(); // blocking for simplicity, consider reactive alternatives
  }

  public MovieDetailsResultDTO getMovieById(String id) {
    return tmdbClient.getWebClient().get()
        .uri(uriBuilder -> uriBuilder
            .path("/movie/{id}")
            .queryParam("append_to_response", "videos,credits,images")
            .build(id))
        .retrieve()
        .onStatus(
            status -> status.value() == 404,
            clientResponse -> {
              logger.warning("Movie not found");
              return Mono.empty();
            })
        .bodyToMono(MovieDetailsDTO.class)
        .map(this::transformResult)
        .onErrorResume(e -> {
          logger.warning("Error making a request to TMDb API: " + e.getMessage());
          return Mono.empty();
        })
        .block(); // blocking for simplicity, consider reactive alternatives
  }

  private MovieDetailsResultDTO transformResult(MovieDetailsDTO movieDetailsDTO) {
    // Perform your transformation here
    MovieDetailsResultDTO transformedMovieDetails = new MovieDetailsResultDTO();

    transformedMovieDetails.setAdult(movieDetailsDTO.isAdult());
    transformedMovieDetails.setBackdropPath(movieDetailsDTO.getBackdropPath());
    transformedMovieDetails.setBudget(movieDetailsDTO.getBudget());
    transformedMovieDetails.setCast(movieDetailsDTO.getCredits().getCast());
    transformedMovieDetails.setCrew(movieDetailsDTO.getCredits().getCrew());
    transformedMovieDetails.setGenres(movieDetailsDTO.getGenres());
    transformedMovieDetails.setHomepage(movieDetailsDTO.getHomepage());
    transformedMovieDetails.setTmdbId(movieDetailsDTO.getId());
    transformedMovieDetails.setImdbId(movieDetailsDTO.getImdbId());
    transformedMovieDetails.setLogos(movieDetailsDTO.getImages().getLogos());
    transformedMovieDetails.setOriginalLanguage(movieDetailsDTO.getOriginalLanguage());
    transformedMovieDetails.setOriginalTitle(movieDetailsDTO.getOriginalTitle());
    transformedMovieDetails.setOverview(movieDetailsDTO.getOverview());
    transformedMovieDetails.setPopularity(movieDetailsDTO.getPopularity());
    transformedMovieDetails.setPosterPath(movieDetailsDTO.getPosterPath());
    transformedMovieDetails.setPosters(movieDetailsDTO.getImages().getPosters());
    transformedMovieDetails.setProductionCompanies(movieDetailsDTO.getProductionCompanies());
    transformedMovieDetails.setProductionCountries(movieDetailsDTO.getProductionCountries());
    transformedMovieDetails.setReleaseDate(movieDetailsDTO.getReleaseDate());
    transformedMovieDetails.setRevenue(movieDetailsDTO.getRevenue());
    transformedMovieDetails.setRuntime(movieDetailsDTO.getRuntime());
    transformedMovieDetails.setSpokenLanguages(movieDetailsDTO.getSpokenLanguages());
    transformedMovieDetails.setStatus(movieDetailsDTO.getStatus());
    transformedMovieDetails.setTagline(movieDetailsDTO.getTagline());
    transformedMovieDetails.setTitle(movieDetailsDTO.getTitle());
    transformedMovieDetails.setVideos(movieDetailsDTO.getVideos().getResults());
    transformedMovieDetails.setVoteAverage(movieDetailsDTO.getVoteAverage());
    transformedMovieDetails.setVoteCount(movieDetailsDTO.getVoteCount());

    return transformedMovieDetails;

  }
}

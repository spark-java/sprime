package spark.sprime.tmdb.movie.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jakarta.annotation.security.RolesAllowed;

import java.util.List;

import spark.sprime.tmdb.movie.dto.*;
import spark.sprime.tmdb.movie.service.MovieService;

@RestController
@RolesAllowed({ "USER", "ADMIN" })
public class MovieController {
  private final MovieService movieService;

  @Autowired
  public MovieController(MovieService movieService) {
    this.movieService = movieService;
  }

  @GetMapping("/tmdb/search/movie")
  public List<MovieDTO> searchMovies(@RequestParam String query) {
    return movieService.searchMovies(query);
  }

  @GetMapping("/tmdb/movie/{id}")
  public MovieDetailsResultDTO getMovieById(@PathVariable String id) {
    return movieService.getMovieById(id);
  }
}

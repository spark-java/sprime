package spark.sprime.tmdb.api;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
public class TmdbClient {

  private final WebClient webClient;

  public TmdbClient(
      @Value("${tmdb.base_url}") String baseUrl,
      @Value("${tmdb.bearer_token}") String tmdbBearerToken,
      WebClient.Builder webClientBuilder) {
    this.webClient = webClientBuilder
        .baseUrl(baseUrl)
        .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + tmdbBearerToken)
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .build();
  }

  public WebClient getWebClient() {
    return webClient;
  }
}

package spark.sprime.tmdb.tv.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

// Nested classes for inner structures
@Data
public class EpisodeDTO {
  private static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/original";

  private int id;
  private String name;
  private String overview;
  @JsonProperty("vote_average")
  private double voteAverage;
  @JsonProperty("vote_count")
  private int voteCount;
  @JsonProperty("air_date")
  private String airDate;
  @JsonProperty("episode_number")
  private int episodeNumber;
  @JsonProperty("episode_type")
  private String episodeType;
  @JsonProperty("production_code")
  private String productionCode;
  private int runtime;
  @JsonProperty("season_number")
  private int seasonNumber;
  @JsonProperty("show_id")
  private int showId;
  @JsonProperty("still_path")
  private String stillPath;

  public String getStillPath() {
    if (this.stillPath != null) {
      return IMAGE_BASE_URL + this.stillPath;
    } else {
      return null;
    }
  }
}
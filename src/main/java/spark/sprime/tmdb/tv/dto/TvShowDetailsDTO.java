package spark.sprime.tmdb.tv.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import spark.sprime.tmdb.commons.*;

import java.util.List;

@Data
public class TvShowDetailsDTO {
  private static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/original";

  private boolean adult;

  @JsonProperty("backdrop_path")
  private String backdropPath;

  @JsonProperty("created_by")
  private List<Object> createdBy;

  @JsonProperty("episode_run_time")
  private List<Integer> episodeRunTime;

  @JsonProperty("first_air_date")
  private String firstAirDate;

  private List<GenreDTO> genres;

  private String homepage;

  private int id;

  @JsonProperty("in_production")
  private boolean inProduction;

  private List<String> languages;

  @JsonProperty("last_air_date")
  private String lastAirDate;

  @JsonProperty("last_episode_to_air")
  private EpisodeDTO lastEpisodeToAir;

  private String name;

  @JsonProperty("next_episode_to_air")
  private Object nextEpisodeToAir;

  private List<NetworkDTO> networks;

  @JsonProperty("number_of_episodes")
  private int numberOfEpisodes;

  @JsonProperty("number_of_seasons")
  private int numberOfSeasons;

  @JsonProperty("origin_country")
  private List<String> originCountry;

  @JsonProperty("original_language")
  private String originalLanguage;

  @JsonProperty("original_name")
  private String originalName;

  private String overview;

  private double popularity;

  @JsonProperty("poster_path")
  private String posterPath;

  @JsonProperty("production_companies")
  private List<ProductionCompanyDTO> productionCompanies;

  @JsonProperty("production_countries")
  private List<ProductionCountryDTO> productionCountries;

  private List<SeasonDTO> seasons;

  @JsonProperty("spoken_languages")
  private List<SpokenLanguageDTO> spokenLanguages;

  private String status;

  private String tagline;

  private String type;

  @JsonProperty("vote_average")
  private double voteAverage;

  @JsonProperty("vote_count")
  private int voteCount;

  private Videos videos;
  private Credits credits;
  private Images images;

  public String getPosterPath() {
    if (this.posterPath != null) {
      return IMAGE_BASE_URL + this.posterPath;
    } else {
      return null;
    }
  }

  public String getBackdropPath() {
    if (this.backdropPath != null) {
      return IMAGE_BASE_URL + this.backdropPath;
    } else {
      return null;
    }
  }
}
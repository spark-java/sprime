package spark.sprime.tmdb.tv.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SeasonDTO {
  private static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/original";

  @JsonProperty("air_date")
  private String airDate;
  @JsonProperty("episode_count")
  private int episodeCount;
  private int id;
  private String name;
  private String overview;
  @JsonProperty("poster_path")
  private String posterPath;
  @JsonProperty("season_number")
  private int seasonNumber;
  @JsonProperty("vote_average")
  private double voteAverage;

  public String getPosterPath() {
    if (this.posterPath != null) {
      return IMAGE_BASE_URL + this.posterPath;
    } else {
      return null;
    }
  }

}
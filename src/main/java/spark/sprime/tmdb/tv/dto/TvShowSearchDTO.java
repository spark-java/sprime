package spark.sprime.tmdb.tv.dto;

import java.util.List;

import lombok.Data;

@Data
public class TvShowSearchDTO {
  private int page;
  private List<TvShowDTO> results;
  private int totalPages;
  private int totalResults;

}
package spark.sprime.tmdb.tv.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jakarta.annotation.security.RolesAllowed;

import java.util.List;

import spark.sprime.tmdb.tv.dto.TvShowDTO;
import spark.sprime.tmdb.tv.dto.TvShowDetailsDTO;
import spark.sprime.tmdb.tv.service.TvService;

@RestController
@RolesAllowed({ "USER", "ADMIN" })
public class TvController {
  private final TvService tvService;

  @Autowired
  public TvController(TvService tvService) {
    this.tvService = tvService;
  }

  @GetMapping("/tmdb/search/tv")
  public List<TvShowDTO> searchTv(@RequestParam String query) {
    return tvService.searchTv(query);
  }

  @GetMapping("/tmdb/tv/{id}")
  public TvShowDetailsDTO getTvShowById(@PathVariable String id) {
    return tvService.getTvShowById(id);
  }
}

package spark.sprime.tmdb.tv.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import reactor.core.publisher.Mono;
import spark.sprime.tmdb.api.TmdbClient;
import spark.sprime.tmdb.tv.dto.TvShowDTO;
import spark.sprime.tmdb.tv.dto.TvShowDetailsDTO;
import spark.sprime.tmdb.tv.dto.TvShowSearchDTO;

/**
 * TvService
 */
@Service
public class TvService {
  private static final Logger logger = Logger.getLogger(TvService.class.getName());
  @Autowired
  private TmdbClient tmdbClient;

  public List<TvShowDTO> searchTv(String query) {
    return tmdbClient.getWebClient().get()
        .uri(uriBuilder -> uriBuilder
            .path("/search/tv")
            .queryParam("query", query)
            .build())
        .retrieve()
        .onStatus(
            status -> status.value() == 404,
            clientResponse -> {
              logger.warning("Movie not found");
              return Mono.empty();
            })
        .bodyToMono(TvShowSearchDTO.class)
        .map(TvShowSearchDTO::getResults)
        .onErrorResume(e -> {
          logger.warning("Error making a request to TMDb API: " + e.getMessage());
          return Mono.just(Collections.emptyList());
        })
        .block(); // blocking for simplicity, consider reactive alternatives
  }

  public TvShowDetailsDTO getTvShowById(String id) {
    String append_to_response = "videos,credits,images";

    return tmdbClient.getWebClient().get()
        .uri(uriBuilder -> uriBuilder
            .path("/tv/{id}")
            .queryParam("append_to_response", append_to_response)
            .build(id))
        .retrieve()
        .onStatus(
            status -> status.value() == 404,
            clientResponse -> {
              logger.warning("Movie not found");
              return Mono.empty();
            })
        .bodyToMono(TvShowDetailsDTO.class)
        .onErrorResume(e -> {
          logger.warning("Error making a request to TMDb API: " + e.getMessage());
          return Mono.empty();
        })
        .block(); // blocking for simplicity, consider reactive alternatives
  }
}
